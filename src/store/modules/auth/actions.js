import Vue from 'vue';
import router from '../../../router';
import * as mutationType from './mutationType';
import * as actionType from './actionType';
import * as menuActionType from '../menu/actionType';
const actions = {
    [actionType.autoLogin]: (context) => {
        if(localStorage.getItem('token')){
            context.commit(mutationType.SET_TOKEN, localStorage.getItem('token'));
            context.dispatch(actionType.updateUser);
        }
    },
    [actionType.logout]: (context) => {
      localStorage.removeItem('token');
        context.dispatch(menuActionType.setMenuGuest);
      context.commit(mutationType.DELETE_USER);
      context.commit(mutationType.DELETE_TOKEN);

      router.push({name: 'Login'});
    },
    [actionType.setToken]: (context, payload) => {
        localStorage.setItem('token', payload);
        context.commit(mutationType.SET_TOKEN, payload);
    },
    [actionType.updateUser]: (context) => {
        Vue.http.get(`${process.env.VUE_APP_URL}users/logged`).then(res => {
            let user = res && res.body ? res.body : undefined;
            context.commit(mutationType.SET_USER, user);
            context.dispatch(menuActionType.setMenuAuth);
        })
    }
};

export default actions;
