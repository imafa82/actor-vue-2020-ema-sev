const getters = {
    getVoices: state => {
        return state.voices;
    }
};

export default getters;
