import * as mutationType from './mutationType';

const mutations = {
    [mutationType.SET_VOICES]: (state, payload) => {
        state.voices = payload;
    }
};

export default mutations;
