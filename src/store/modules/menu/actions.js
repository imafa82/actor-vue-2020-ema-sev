import routes from '../../../router/authRoutes'
import * as mutationType from './mutationType';
import * as actionType from './actionType';

const actions = {
    [actionType.setVoices]: (context, payload) => {
        context.commit(mutationType.SET_VOICES, payload)
    },
    [actionType.setMenuAuth]: context => {
        const voices = routes.filter(route => route.menuTitle);
        context.dispatch(actionType.setVoices, voices);
    },
    [actionType.setMenuGuest]: context => {
        context.dispatch(actionType.setVoices, []);
    }
};

export default actions;
