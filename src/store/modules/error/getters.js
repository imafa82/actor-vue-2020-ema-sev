const getters = {
    getErrors: state => {
        return state.errors;
    }
};

export default getters;
