import Vue from 'vue'
import Vuex from 'vuex'
import auth from "./modules/auth";
import menu from "./modules/menu";
import error from "./modules/error";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    auth,
    menu,
    error
  }
})
