import store from '../store/index'
import * as actionType from '../store/modules/auth/actionType';
export default (request, next) => {
    const token = store.getters.getToken;
    if(token){
        request.headers.set('Authorization', token);
    }
    next(response => {
        if(response.status === 401)
            store.dispatch(actionType.logout);
    });
}
