import authRoutes from "./authRoutes";
import AuthGuard from "../guards/authGuard";
import NoAuthGuard from "../guards/noAuthGuard";

const routes = [
    {
        path: '/',
        beforeEnter: NoAuthGuard,
        name: 'Login',
        component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
    },
    {
        path: '/admin',
        beforeEnter: AuthGuard,
        component: () => import(/* webpackChunkName: "auth" */ '../views/AuthPage.vue'),
        children: authRoutes
    }
];

export default routes;
