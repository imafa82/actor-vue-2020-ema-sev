const authRoutes = [
    {
        path: '/',
        name: 'Home',
        menuTitle: 'Home',
        component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
    },
    {
        path: '/actors',
        name: 'Actors',
        menuTitle: 'Attori',
        component: () => import(/* webpackChunkName: "actors" */ '../views/Actors.vue')
    },
    {
        path: '/about',
        name: 'About',
        menuTitle: 'About',
        component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    }
];

export default authRoutes;
